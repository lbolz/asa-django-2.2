from django.shortcuts import render
from newsletter.forms import NewsletterUserForm
from newsletter.models import NewsletterUser
# Create your views here.

def home(request):
	form = NewsletterUserForm(request.POST or None)

	context = {
		"form": form
	}
	if form.is_valid():
		instance = form.save(commit=False)
		full_name = form.cleaned_data.get('full_name')
		if not full_name:
			full_name = "No Name"
		instance.full_name = full_name
		instance = form.save()
		context = {
			"title":"Thank You!"
		}
	if request.user.is_authenticated and request.user.is_staff:
		# print(NewsletterUser.objects.all())
		# for instance in NewsletterUser.objects.all():
		# 	print(instance.fullname)
		queryset = NewsletterUser.objects.all().order_by('-timestamp')
		context = {
			"queryset": queryset
		}

	return render(request, "home.html", context)

def about(request):
    return render(request, 'about.html', {})

def contact(request):
    return render(request, 'contact.html', {})
