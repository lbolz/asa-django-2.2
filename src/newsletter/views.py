from django.shortcuts import render
from .models import NewsletterUser
# Create your views here.

def signups_list(request):
	queryset = "Sorry, you're not an authorized user"
	context = {
		'queryset': queryset
	}
	if request.user.is_authenticated and request.user.is_staff:
		queryset = NewsletterUser.objects.all().order_by('-timestamp')
		context = {
			'queryset': queryset
		}
	return render(request, "newsletter/signups_list.html", context)
