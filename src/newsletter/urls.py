from django.urls import path, include
from .views import signups_list

urlpatterns = [
    path('signups/', signups_list, name="signups"),
]
