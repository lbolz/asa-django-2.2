from django import forms
from .models import NewsletterUser

class NewsletterUserForm(forms.ModelForm):
    class Meta:
        model = NewsletterUser
        fields = ['full_name', 'email']

    def clean_name(self):
        name = self.cleaned_data.get('full_name')
        return name

    def clean_email(self):
        email = self.cleaned_data.get('email')
        return email
