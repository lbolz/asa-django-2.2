from django.contrib import admin
from .forms import NewsletterUserForm
from .models import NewsletterUser

class NewsletterUserAdmin(admin.ModelAdmin):
	list_display = ["__str__", "timestamp", "updated"]
	form = NewsletterUserForm

# Register your models here.
admin.site.register(NewsletterUser, NewsletterUserAdmin)
